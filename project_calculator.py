#Hello World
def add_two_numbers(num1, num2):
    return num1 + num2

def subtract_two_numbers(num1, num2):
    return num1 - num2

def multiply_two_numbers(num1, num2):
    return num1 * num2

def divide_two_numbers(num1, num2):
    if num2 == 0:
        print("Ділення на нуль не можливе")
        return None
    return num1 / num2

def power_of_number(base, exponent):
    return base ** exponent

def gcd(a, b):
    while b != 0:
        a, b = b, a % b
    return a

def lcm(a, b):
    return abs(a*b) // gcd(a, b) if a and b else 0

def main():
    num1 = int(input("Введіть перше число: "))
    num2 = int(input("Введіть друге число: "))
    operation = input("Введіть дію (+, -, *, /, **, gcd або lcm): ")

    if operation == "+":
        result = add_two_numbers(num1, num2)
    elif operation == "-":
        result = subtract_two_numbers(num1, num2)
    elif operation == "*":
        result = multiply_two_numbers(num1, num2)
    elif operation == "/":
        result = divide_two_numbers(num1, num2)
    elif operation == "**":
        result = power_of_number(num1, num2)
    elif operation == "gcd":
        result = gcd(num1, num2)
    elif operation == "lcm":
        result = lcm(num1, num2)
    else:
        print("Непідтримувана дія")
        return

    print("Результат:", result)

if __name__ == "__main__":
    main()



    


import unittest

class TestSumFunction(unittest.TestCase):

    def test_sum_positive_numbers(self):
        self.assertEqual(add_two_numbers(1, 2), 3)

    def test_sum_negative_numbers(self):
        self.assertEqual(add_two_numbers(-1, -2), -3)

    def test_sum_mixed_numbers(self):
        self.assertEqual(add_two_numbers(-1, 2), 1)
        self.assertEqual(add_two_numbers(1, -2), -1)

        if __name__ == '__main__': 
            unittest.main()
        
        import unittest

class TestSubtractTwoNumbers(unittest.TestCase):

    def test_subtract_positive_numbers(self):
        result = subtract_two_numbers(5, 3)
        self.assertEqual(result, 2)

    def test_subtract_negative_numbers(self):
        result = subtract_two_numbers(-5, -3)
        self.assertEqual(result, -2)

    def test_subtract_mixed_numbers(self):
        result = subtract_two_numbers(5, -3)
        self.assertEqual(result, 8)

if __name__ == '__main__':
    unittest.main()

    import unittest
from your_program import multiply_two_numbers

class TestMultiplyTwoNumbers(unittest.TestCase):

    def test_multiply_positive_numbers(self):
        result = multiply_two_numbers(5, 3)
        self.assertEqual(result, 15)

    def test_multiply_negative_numbers(self):
        result = multiply_two_numbers(-5, -3)
        self.assertEqual(result, 15)

    def test_multiply_mixed_numbers(self):
        result = multiply_two_numbers(-5, 3)
        self.assertEqual(result, -15)

    def test_multiply_by_zero(self):
        result = multiply_two_numbers(5, 0)
        self.assertEqual(result, 0)

if __name__ == '__main__':
    unittest.main()
import unittest
from your_program import divide_two_numbers

class TestDivideTwoNumbers(unittest.TestCase):

    def test_divide_positive_numbers(self):
        result = divide_two_numbers(6, 3)
        self.assertEqual(result, 2)

    def test_divide_negative_numbers(self):
        result = divide_two_numbers(-6, -3)
        self.assertEqual(result, 2)

    def test_divide_mixed_numbers(self):
        result = divide_two_numbers(-6, 3)
        self.assertEqual(result, -2)

    def test_divide_by_zero(self):
        result = divide_two_numbers(5, 0)
        self.assertIsNone(result)
        # Або можна було б написати:
        # self.assertRaises(ZeroDivisionError, divide_two_numbers, 5, 0)

if __name__ == '__main__':
    unittest.main()
    
    import unittest
from your_program import power_of_number

class TestPowerOfNumber(unittest.TestCase):

    def test_positive_exponent(self):
        result = power_of_number(2, 3)
        self.assertEqual(result, 8)

    def test_negative_exponent(self):
        result = power_of_number(2, -3)
        self.assertEqual(result, 0.125)

    def test_zero_exponent(self):
        result = power_of_number(5, 0)
        self.assertEqual(result, 1)

if __name__ == '__main__':
    unittest.main()

import unittest
from your_program import gcd

class TestGCD(unittest.TestCase):

    def test_gcd_of_10_and_25(self):
        result = gcd(10, 25)
        self.assertEqual(result, 5)

    def test_gcd_of_14_and_28(self):
        result = gcd(14, 28)
        self.assertEqual(result, 14)

    def test_gcd_of_17_and_31(self):
        result = gcd(17, 31)
        self.assertEqual(result, 1)

    def test_gcd_of_0_and_5(self):
        result = gcd(0, 5)
        self.assertEqual(result, 5)

    def test_gcd_of_0_and_0(self):
        result = gcd(0, 0)
        self.assertEqual(result, 0)

if __name__ == '__main__':
    unittest.main()

    import unittest
from your_program import lcm

class TestLCM(unittest.TestCase):

    def test_lcm_of_4_and_6(self):
        result = lcm(4, 6)
        self.assertEqual(result, 12)

    def test_lcm_of_15_and_25(self):
        result = lcm(15, 25)
        self.assertEqual(result, 75)

    def test_lcm_of_7_and_11(self):
        result = lcm(7, 11)
        self.assertEqual(result, 77)

    def test_lcm_of_0_and_5(self):
        result = lcm(0, 5)
        self.assertEqual(result, 0)

    def test_lcm_of_0_and_0(self):
        result = lcm(0, 0)
        self.assertEqual(result, 0)

if __name__ == '__main__':
    unittest.main()

